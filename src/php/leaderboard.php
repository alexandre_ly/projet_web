<!DOCTYPE HTML>
<head>
    <meta charset="utf-8" />
    <link rel="stylesheet" type="text/css" href="../css/index.css"/>
    <link rel="stylesheet" type="text/css" href="../css/leaderboard.css"/>
    <link href="https://fonts.googleapis.com/css?family=Press+Start+2P" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Black+Ops+One" rel="stylesheet">
    <script type="text/javascript" src="../js/leaderboard.js"></script>
</head>

<body>
  <div id="banniere">
    <img src="../image/head.png" width="100%" height="6%">
  </div>
  <div class="navbar" id="mynavbar">
    <ul>
        <a href="../html/index.html">Home</a>
        <a href="../php/leaderboard.php">Leaderboard</a>
        <a href="../html/contact.html">Contact</a>
      </ul> 
  </div>
    
 <div id="leaderboard" align="center">
     <div id="Titre">
        <h1> Leaderboard</h1>
     </div>
</div>
  <?php
    try {
        $bdd = new PDO('mysql:host=localhost:8889;dbname=girafe;charset=utf8', 'root',
            'root', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
    }
    catch (Exception $e)
    {
        die('Erreur : '.$e->getMessage());
    }
    $request = $bdd->query('SELECT * FROM LEADERBOARD ORDER BY score DESC LIMIT 10');
?>
        <table>
            <thead>
                <tr>
                 <th> Pseudo </th>
                 <th> Score </th>
                </tr>
            </thead>
            <tbody>
    <?php while ($tuple = $request->fetch())
    { ?>
            <tr>
                <td align="center"> <?php echo $tuple['PSEUDO']; ?> </td>
                <td align="center"> <?php echo $tuple['SCORE']; ?> </td>
            </tr>
  <?php
    }
    $request->closeCursor(); ?>
            </tbody>
        </table>
</body>