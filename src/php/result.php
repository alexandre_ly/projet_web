<?php
session_start();
?>

<!DOCTYPE HTML>
<head>
    <meta charset="utf-8" />
    <link rel="stylesheet" type="text/css" href="../css/index.css"/>
    <link rel="stylesheet" type="text/css" href="../css/result.css"/>
    <link href="https://fonts.googleapis.com/css?family=Press+Start+2P" rel="stylesheet">
    <script type="text/javascript" src="../js/index.js"></script>
</head>

<body>
<div id="banniere">
    <img src="../image/head.png" width="100%" height="6%">
</div>
<div class="navbar" id="mynavbar">
    <ul>
        <a href="../html/index.html">Home</a>
        <a href="../php/leaderboard.php">Leaderboard</a>
        <a href="../html/contact.html">Contact</a>
    </ul>
</div>

<br/>

<div id="title1" align="center">
    Your Results
</div>
    <br/>

    <div id="pseudo" align="center"> <?php $pseudo = $_SESSION['pseudo'];
                                        echo $pseudo ?> : </div>
    <br/>
    <div id="score" align="center"> <?php $score = $_GET['score'];
                           echo $score;
                        ?> </div>


    <?php
    try {
        $bdd = mysqli_connect("localhost:8889", "root", "root");
        $db_selected = mysqli_select_db($bdd, 'girafe');
    }
    catch (Exception $e) {
        die('Erreur : ' . $e->getMessage());
    }

    $sql = "INSERT INTO LEADERBOARD(pseudo, score) VALUES ('$pseudo', '$score')";

    if (mysqli_query($bdd, $sql)) {
        mysqli_close($bdd);
    }
    ?>

    <button onclick="replay()" align="center">Replay ? </button>
</body>

<script>
function replay(){
    window.location.href = "../html/game.html";
}
</script>
