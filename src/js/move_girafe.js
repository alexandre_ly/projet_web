KEY_SPACE			= 32;
KEY_DOWN	        = 40;
KEY_UP		        = 38;
KEY_LEFT	        = 37;
KEY_RIGHT	        = 39;
var           score = 0;
var           game = 1;

function checkEventObj(_event_){
    if (window.event)
        return window.event;
    else
        return _event_;
}

function jump()
{
    var value = parseInt(document.getElementById("gir").style.bottom, 10); // 10 = Le nombre en decimal a parser.
    var id2 = setInterval(frame2, 10);
    function frame2(){
        if (value == 0) {
            clearInterval(id2);
        }
        else {
            value = value - 1;
            document.getElementById("gir").style.bottom = (value).toString() + "px";
        }
    }
    var id = setInterval(frame, 3);
    function frame() {
        if (value == 60) {
            clearInterval(id);
        }
        else {
            value = value + 1;
            document.getElementById("gir").style.bottom = (value).toString() + "px";
        }
    }
}

function move_obs() {
    var value = parseInt(document.getElementById("obs").style.right, 10);
    var id = setInterval(frame1, 1);
    function frame1(){
        if (value == (window.innerWidth + 60)) {
            document.getElementById('obs').style.right = '0px';
            score = score + 10;
            clearInterval(id);
            move_obs();
        }
        else
        {
            value = value + 1;
            var girafe = document.getElementById('gir').getBoundingClientRect();
            var feuille = document.getElementById('obs').getBoundingClientRect();
            if ((value % 100) == 1)
                score++;
            if (feuille.left > 0 && feuille.left < 68 && (girafe.bottom > 720))
            {
                // Si la girafe est en jump ou ne jump pas, et touche la feuille.
                var box = confirm("\nGAME OVER\nScore = " + score);
                if (box == true || box == false) {
                    clearInterval(id);
                   window.location.href = "../php/result.php?score=" + score;
                   return;
                }
            }
            document.getElementById('obs').style.right = (value).toString() + "px";
        }
    }
}

function create_obs(){
    var obs = new Image(50, 50);
    var overlay = document.getElementById('overlay');
    obs.src = "../image/feuille.png";
    obs.id = "obs";
    obs.style.bottom = 0;
    obs.style.right = 0;
    overlay.appendChild(obs);
}


function applyKey(_event_)
{
    var winObj = checkEventObj(_event_); // Permet de verifier si un evenement a été effectué
    var intKeyCode = winObj.keyCode; // Permet de recupéré la valeur de l'evenment.

    if (intKeyCode == KEY_SPACE) // Si on a appuyé sur la touche ESPACE
    {
        jump(); // On appel a la fonction jump (saute).
    }
}